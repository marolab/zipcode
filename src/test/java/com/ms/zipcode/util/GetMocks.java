package com.ms.zipcode.util;

import com.ms.zipcode.domain.CachesSearches;
import com.ms.zipcode.domain.City;
import com.ms.zipcode.domain.Resume;
import com.ms.zipcode.domain.Zipcode;
import com.ms.zipcode.domain.dto.ZipcodeDTO;
import com.ms.zipcode.domain.dto.ZipcodeIdDTO;
import org.bson.types.ObjectId;

import java.util.ArrayList;

public abstract class GetMocks {
    public static String STRING_VALUE_MOCK = "mock";
    public static String STRING_VALUE_CEP_MOCK = "99999-999";
    public static String STRING_VALUE_CEP_INT_MOCK = "99999999";
    public static String STRING_VALUE_HEX_MOCK = "5c9b9a0289d2b311b150b92c";
    public static long LONG_VALUE_MOCK = 1L;
    public static int INT_VALUE_CEP_MOCK = 99999999;

    public static Zipcode getZipcodeMock() {
        return Zipcode.builder()
                ._id(new ObjectId(new ObjectId(STRING_VALUE_HEX_MOCK).toHexString()))
                .uf(STRING_VALUE_MOCK)
                .cep(STRING_VALUE_CEP_MOCK)
                .bairro(STRING_VALUE_MOCK)
                .ddd(STRING_VALUE_MOCK)
                .complemento(STRING_VALUE_MOCK)
                .ibge(STRING_VALUE_MOCK)
                .siafi(STRING_VALUE_MOCK)
                .localidade(STRING_VALUE_MOCK)
                .logradouro(STRING_VALUE_MOCK)
                .build();
    }

    public static ZipcodeDTO getZipcodeDTOMock() {
        return ZipcodeDTO.builder()
                .zipcodeId(getZipcodeIdDTO())
                .zipcode(STRING_VALUE_CEP_MOCK)
                .id(Integer.parseInt(STRING_VALUE_CEP_INT_MOCK))
                .district(STRING_VALUE_MOCK)
                .objectId(new ObjectId(STRING_VALUE_HEX_MOCK).toHexString())
                .build();
    }

    public static ZipcodeIdDTO getZipcodeIdDTO() {
        return ZipcodeIdDTO.builder()
                .city(STRING_VALUE_MOCK)
                .street(STRING_VALUE_MOCK)
                .uf(STRING_VALUE_MOCK)
                .build();
    }

    public static Resume getResumeMock() {
        return Resume.builder()
               .objectId(new ObjectId(STRING_VALUE_HEX_MOCK))
               .resumeId(STRING_VALUE_CEP_INT_MOCK)
               .url(STRING_VALUE_MOCK).build();
    }

    public static CachesSearches getCachesSearches() {
        var zipcodeList = new ArrayList<ZipcodeDTO>();
        zipcodeList.add(getZipcodeDTOMock());
        return CachesSearches.builder()
                .zipcodeDTOS(zipcodeList)
                .url(STRING_VALUE_MOCK)
                .id(LONG_VALUE_MOCK).build();
    }

    public static City getCityMock() {
        return City.builder()
                .id(LONG_VALUE_MOCK)
                .uf(STRING_VALUE_MOCK)
                .cityName(STRING_VALUE_MOCK)
                .country(STRING_VALUE_MOCK).build();

    }

}

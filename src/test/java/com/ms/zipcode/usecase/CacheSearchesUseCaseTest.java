package com.ms.zipcode.usecase;

import com.ms.zipcode.domain.CachesSearches;
import com.ms.zipcode.domain.Zipcode;
import com.ms.zipcode.domain.dto.ZipcodeDTO;
import com.ms.zipcode.storage.CacheSearchesStorage;
import com.ms.zipcode.util.GetMocks;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ms.zipcode.util.GetMocks.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CacheSearchesUseCaseTest {

    @InjectMocks
    private CacheSearchesUseCase useCase;
    @Mock
    private ResumeUseCase resumeUseCase;
    @Mock
    private CacheSearchesStorage storage;

    @Test
    public void saveCache_test() {
        var resume = getCachesSearches();
        when(storage.save(any())).thenReturn(resume);
        useCase.saveCache(resume);
        verify(storage, times(1)).save(any());
    }

    @Test
    public void findAll_test() {
        var list = new ArrayList<CachesSearches>();
        list.add(getCachesSearches());
        when(storage.findAll()).thenReturn(list);
        var listReturn = useCase.findAll();
        assertEquals(listReturn.get(0).getId(), list.get(0).getId());
    }

    @Test
    public void cachePurge_test() {
        doNothing().when(storage).deleteAll();
        useCase.cachePurge();
        verify(storage, times(1)).deleteAll();
    }

    @Test
    public void findByUrl_test() {
        var opt = Optional.of(getCachesSearches());
        when(storage.findByUrl(anyString())).thenReturn(opt);
        var optExpected = useCase.findByUrl(GetMocks.STRING_VALUE_MOCK);
        assertTrue(optExpected.isPresent());
        assertEquals(optExpected.get().getId(), optExpected.get().getId());
    }

    @Test
    public void update_test() {
        var opt = Optional.of(getCachesSearches());
        when(storage.findById(anyLong())).thenReturn(opt);
        List<ZipcodeDTO> listDto = new ArrayList<>();
        listDto.add(getZipcodeDTOMock());
        useCase.update(LONG_VALUE_MOCK, listDto);
        verify(storage, times(1)).findById(anyLong());
        verify(storage, times(1)).save(any());

    }

    @Test
    public void parseCustom_test() {
        var expected = useCase.parseCustom(STRING_VALUE_CEP_MOCK);
        Assertions.assertEquals(expected, INT_VALUE_CEP_MOCK);
    }

    @Test
    public void refreshCache_test() {
        // Optional<Resume> resume = resumeUseCase.findByResumeId(zipcode.get_id().toString())
        var resumeOpt = Optional.of(getResumeMock());
        List<Zipcode> zipcodeList = new ArrayList<>();
        zipcodeList.add(GetMocks.getZipcodeMock());
        when(resumeUseCase.findByResumeId(anyString())).thenReturn(resumeOpt);
        useCase.refreshCache(zipcodeList);
        verify(resumeUseCase, times(1)).findByResumeId(anyString());
    }

    @Test
    public void refreshCache_Update_test() {
        // Optional<Resume> resume = resumeUseCase.findByResumeId(zipcode.get_id().toString())
        var resumeOpt = Optional.of(getResumeMock());
        List<Zipcode> zipcodeList = new ArrayList<>();
        zipcodeList.add(GetMocks.getZipcodeMock());
        zipcodeList.add(GetMocks.getZipcodeMock());
        when(resumeUseCase.findByResumeId(anyString())).thenReturn(resumeOpt);
        useCase.refreshCache(zipcodeList);
        verify(resumeUseCase, times(2)).findByResumeId(anyString());
    }

}
package com.ms.zipcode.usecase;

import com.ms.zipcode.domain.Zipcode;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.ms.zipcode.util.GetMocks.*;

class ZicodeDTOUseCaseTest {
    private ZicodeDTOUseCase zicodeDTOUseCase;

    @BeforeEach
    public void setUp() {
        zicodeDTOUseCase = new ZicodeDTOUseCase();
    }

    @Test
    public void mapperZipcodeDTO_Test() {
        var zipcodeMock = getZipcodeMock();
        var dtoMock = getZipcodeDTOMock();
        var mockResult = zicodeDTOUseCase.mapperZipcodeDTO(zipcodeMock, STRING_VALUE_MOCK);
        Assertions.assertEquals(mockResult, dtoMock);
        Assertions.assertEquals(mockResult.getId(), dtoMock.getId());
        Assertions.assertNotNull(mockResult);
    }

    @Test
    public void parseCustom_Test() {
        var zipcode = Zipcode.builder()
                .cep(STRING_VALUE_CEP_MOCK)
                .uf(STRING_VALUE_MOCK)
                ._id(new ObjectId())
                .localidade(STRING_VALUE_MOCK)
                .logradouro(STRING_VALUE_MOCK).build();
        var zipcodeDTOMock = zicodeDTOUseCase.mapperZipcodeDTO(zipcode, STRING_VALUE_MOCK);
        Assertions.assertNotNull(zipcodeDTOMock);

    }

    @Test
    public void mapperZipcodeDTO_Test_With_Id_Zero() {
        var zipcode = Zipcode.builder()
                .cep("")
                .uf(STRING_VALUE_MOCK)
                ._id(new ObjectId())
                .localidade(STRING_VALUE_MOCK)
                .logradouro(STRING_VALUE_MOCK).build();
        var zipcodeDTOMock = zicodeDTOUseCase.mapperZipcodeDTO(zipcode, STRING_VALUE_MOCK);
        Assertions.assertEquals(zipcodeDTOMock.getId(), 0);
    }

    @Test
    public void mapperZipcodeDTO_Test_Failed() {
        var zipcode = Zipcode.builder()
                .cep(STRING_VALUE_CEP_INT_MOCK)
                .uf(STRING_VALUE_MOCK)
                ._id(new ObjectId())
                .localidade(STRING_VALUE_MOCK)
                .logradouro(STRING_VALUE_MOCK).build();
        var zipcodeDTOMock = zicodeDTOUseCase.mapperZipcodeDTO(zipcode, STRING_VALUE_MOCK);
        Assertions.assertEquals(zipcodeDTOMock.getId(), Integer.parseInt(STRING_VALUE_CEP_INT_MOCK));
    }

}
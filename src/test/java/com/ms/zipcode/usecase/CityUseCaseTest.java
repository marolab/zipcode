package com.ms.zipcode.usecase;

import com.google.gson.Gson;
import com.ms.zipcode.configuration.ResourcesConfig;
import com.ms.zipcode.storage.CityStorage;
import com.ms.zipcode.util.GetMocks;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ms.zipcode.util.GetMocks.STRING_VALUE_MOCK;
import static com.ms.zipcode.util.GetMocks.getCityMock;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CityUseCaseTest {
    @InjectMocks
    private CityUseCase useCase;
    @Mock
    private CityStorage storage;
    @Mock
    private ResourcesConfig resourcesConfig;

    @Test
    public void initLoadData_test() {
        useCase.initLoadData();
        verify(resourcesConfig, times(1)).loadCityData();
    }

    @Test
    public void validationUfAndCity_test() {
        var city = Optional.of(getCityMock());
        Mockito.when(storage.findByName(anyString(), anyString())).thenReturn(city);
        var result = useCase.validationUfAndCity(STRING_VALUE_MOCK, STRING_VALUE_MOCK);
        Assertions.assertTrue(result);
    }

    @Test
    public void save_test() {
        var gson = new Gson();
        useCase.save(gson.toJson(getCityMock()));
        verify(storage, times(1)).save(Mockito.any());
    }




}
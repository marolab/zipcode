package com.ms.zipcode.usecase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import static com.ms.zipcode.util.GetMocks.STRING_VALUE_MOCK;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
class KafkaUseCaseTest {

    @InjectMocks
    private KafkaUseCase useCase;

    @Mock
    private KafkaTemplate<String, String> template;

    @Test
    public void sendoToKafka_test() {
        useCase.sendToKafka(true, STRING_VALUE_MOCK);
        Mockito.verify(template, Mockito.times(1)).send(any(), anyString());
    }
}
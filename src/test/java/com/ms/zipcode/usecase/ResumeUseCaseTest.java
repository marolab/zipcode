package com.ms.zipcode.usecase;

import com.ms.zipcode.domain.Resume;
import com.ms.zipcode.storage.ResumeStorage;
import com.ms.zipcode.util.GetMocks;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ResumeUseCaseTest {
    @InjectMocks
    private ResumeUseCase useCase;
    @Mock
    private ResumeStorage storage;

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void findAll_test() {
        List<Resume> resumes = new ArrayList<>();
        resumes.add(GetMocks.getResumeMock());
        when(storage.findAll()).thenReturn(resumes);
        resumes = useCase.findAll();
        assertNotNull(resumes);
    }

    @Test
    public void save_test() {
        var resume = GetMocks.getResumeMock();
        when(storage.save(any())).thenReturn(resume);
        useCase.save(resume);
        verify(storage, times(1)).save(any());
    }

    @Test
    public void findByUrl_test() {
        Optional<Resume> optionalResume = Optional.of(GetMocks.getResumeMock());
        when(storage.findByUrl(any())).thenReturn(optionalResume);
        Optional<Resume> result = useCase.findByUrl(GetMocks.STRING_VALUE_MOCK);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, optionalResume);
        Assertions.assertTrue(result.isPresent());
    }

    @Test
    public void findById_test() {
        Optional<Resume> optionalResume = Optional.of(GetMocks.getResumeMock());
        when(storage.findById(any())).thenReturn(optionalResume);
        Optional<Resume> result = useCase.findById(GetMocks.STRING_VALUE_MOCK);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, optionalResume);
        Assertions.assertTrue(result.isPresent());
    }

    @Test
    public void findByResumeId_test() {
        Optional<Resume> optionalResume = Optional.of(GetMocks.getResumeMock());
        when(storage.findByResumeId(any())).thenReturn(optionalResume);
        Optional<Resume> result = useCase.findByResumeId(GetMocks.STRING_VALUE_MOCK);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, optionalResume);
        Assertions.assertTrue(result.isPresent());
    }
}
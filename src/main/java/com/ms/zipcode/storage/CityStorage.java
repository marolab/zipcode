package com.ms.zipcode.storage;

import com.ms.zipcode.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CityStorage extends JpaRepository<City, Long> {
    @Query(nativeQuery = true, value = "select id, city_name, country, uf from CITY where uf = ?1 and city_name = ?2")
    Optional<City> findByName(String uf, String name);

}


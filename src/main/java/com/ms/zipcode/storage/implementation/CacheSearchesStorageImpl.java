package com.ms.zipcode.storage.implementation;

import com.ms.zipcode.domain.CachesSearches;
import com.ms.zipcode.domain.dto.ZipcodeDTO;
import com.ms.zipcode.storage.CacheSearchesStorage;
import io.lettuce.core.RedisException;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@EnableRedisRepositories
public abstract class CacheSearchesStorageImpl implements CacheSearchesStorage {
    private static final String HASH_KEY = "CachesSearches";
    private HashOperations operations;
    private RedisTemplate<String, CachesSearches> template;

    CacheSearchesStorageImpl(RedisTemplate<String, CachesSearches> redisTemplate) {
        this.template = redisTemplate;
        operations = this.template.opsForHash();
    }

    public void saveCache(CachesSearches cachesSearches) {
        operations.put(HASH_KEY, cachesSearches.getId(), cachesSearches);
    }

    @Override
    public Optional<CachesSearches> findByUrl(String url){
        Optional<CachesSearches> cachesSearches = (Optional<CachesSearches>) operations.get(HASH_KEY, url);
        return cachesSearches;
    }

    public void deleteAll() {
        operations.delete(HASH_KEY);
    }

    public Optional<CachesSearches> findByCacheId(long id) {
        return (Optional<CachesSearches>) operations.get(HASH_KEY, id);
    }



}

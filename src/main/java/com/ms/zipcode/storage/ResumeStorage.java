package com.ms.zipcode.storage;

import com.ms.zipcode.domain.Resume;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ResumeStorage extends MongoRepository<Resume, String> {
    Optional<Resume> findByUrl(String url);
    Optional<Resume> findByResumeId(String iresumeId);
}

package com.ms.zipcode.storage;

import com.ms.zipcode.domain.Zipcode;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZipcodeStorage extends MongoRepository<Zipcode, String> {
}

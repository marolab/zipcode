package com.ms.zipcode.storage;

import com.ms.zipcode.domain.CachesSearches;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CacheSearchesStorage extends CrudRepository<CachesSearches, Long> {
    void saveCache(CachesSearches cachesSearches) throws Exception;
    Optional<CachesSearches> findByUrl(String url);
    void deleteAll();
}

package com.ms.zipcode.resources;

import com.ms.zipcode.usecase.CoreZipcodeUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1")
public class ZipcodeController {

    @Autowired
    private CoreZipcodeUseCase useCase;

    @GetMapping
    public ResponseEntity<String> search(
            @RequestParam String uf,
            @RequestParam String city,
            @RequestParam String street) {
        return useCase.search(uf, city, street);
    }

}

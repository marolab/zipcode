package com.ms.zipcode.resources;

import com.ms.zipcode.usecase.CityUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1/city")
public class CityController {

    @Autowired
    private CityUseCase useCase;

    @PostMapping
    public void save(@RequestBody String body) {
        useCase.save(body);
    }

}

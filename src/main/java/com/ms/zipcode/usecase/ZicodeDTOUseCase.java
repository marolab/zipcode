package com.ms.zipcode.usecase;

import com.ms.zipcode.domain.Zipcode;
import com.ms.zipcode.domain.dto.ZipcodeDTO;
import com.ms.zipcode.domain.dto.ZipcodeIdDTO;
import org.springframework.stereotype.Service;

@Service
public class ZicodeDTOUseCase {
    public ZipcodeDTO mapperZipcodeDTO(Zipcode zipcode, String url) {
        return ZipcodeDTO.builder()
                .zipcodeId(ZipcodeIdDTO.builder()
                        .city(zipcode.getLocalidade())
                        .street(zipcode.getLogradouro())
                        .uf(zipcode.getUf()).build())
                .district(zipcode.getBairro())
                .zipcode(zipcode.getCep())
                .id(parseIntCustom(zipcode.getCep()))
                .objectId(zipcode.get_id().toString()).build();
    }
    private int parseIntCustom(String value) {
        if (value.isEmpty() || value.isBlank()) {
            return 0;
        } else {
            if (!value.contains("-")) {
                return Integer.parseInt(value);
            } else {
                return Integer.parseInt(value.replace("-", ""));
            }
        }
    }
}

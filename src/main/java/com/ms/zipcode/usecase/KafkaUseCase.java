package com.ms.zipcode.usecase;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class KafkaUseCase {
    @Value("${spring.kafka.producer.topic-zipcode}")
    private String topic;
    @Value("${spring.kafka.producer.topic-zipcode-resum}")
    private String topicResum;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendToKafka(Boolean topicResum, String msn) {
        this.send((topicResum? this.topicResum: this.topic), msn);
    }

    protected void send(String topic, String msn) {
        kafkaTemplate.send(topic, msn);
    }
}

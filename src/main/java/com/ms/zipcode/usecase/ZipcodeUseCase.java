package com.ms.zipcode.usecase;

import com.ms.zipcode.domain.CachesSearches;
import com.ms.zipcode.domain.Resume;
import com.ms.zipcode.domain.Zipcode;
import com.ms.zipcode.domain.dto.ZipcodeDTO;
import com.ms.zipcode.storage.ZipcodeStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ZipcodeUseCase {

    @Autowired
    private CacheSearchesUseCase cacheSearchesUseCase;

    @Autowired
    private ZipcodeStorage zipcodeStorage;
    @Autowired
    private ZicodeDTOUseCase zicodeDTOUseCase;
    @Autowired
    private ResumeUseCase resumeUseCase;
    @Autowired
    private KafkaUseCase kafkaUseCase;



    public void deleteZipcode(Zipcode zipcode) {
        zipcodeStorage.delete(zipcode);
    }

    public void deleteByObjectId(String id) {
        zipcodeStorage.deleteById(id);
    }

    public List<Zipcode> zipcodeFindAll() {
        return zipcodeStorage.findAll();
    }

    public Optional<Zipcode> findById(String id) {
        return zipcodeStorage.findById(id);
    }

    public void saveZipcodeAndSendToKafka(String url, Zipcode[] zipcodes) {
        /* salva o payload na collection MongoDb */
        List<Zipcode> zipcodeList = Arrays.asList(zipcodes);
        zipcodeStorage.saveAll(zipcodeList);
        /* envia a Url para fila do Kafka, a ser consumida por outro serviço */
        kafkaUseCase.sendToKafka(false, url);
        List<ZipcodeDTO> zipcodeDTOS = new ArrayList<>();

        for (Zipcode zipcode: zipcodes) {
            zipcodeDTOS.add(zicodeDTOUseCase.mapperZipcodeDTO(zipcode, url));
            /* collection responsável por fazer D-PARA para refresh do que está em Cache */
            resumeUseCase.save(
                    Resume.builder()
                            .resumeId(zipcode.get_id().toString())
                            .url(url)
                            .build()
            );
        }
        /* armazena no Redis para verificação posterior */
        CachesSearches cachesSearches = CachesSearches.builder()
                        .id(UUID.randomUUID().hashCode())
                        .url(url)
                        .zipcodeDTOS(zipcodeDTOS)
                        .build();
        try {
            cacheSearchesUseCase.saveCache(cachesSearches);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

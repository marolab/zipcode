package com.ms.zipcode.usecase;

import com.google.gson.Gson;
import com.ms.zipcode.domain.AnswersEnum;
import com.ms.zipcode.domain.Zipcode;
import com.ms.zipcode.domain.dto.ZipcodeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CoreZipcodeUseCase {
    @Value("${zipcode.url-root}")
    private String urlRoot;

    @Autowired
    private CityUseCase cityUseCase;
    @Autowired
    private ZipcodeUseCase zipcodeUseCase;
    @Autowired
    private CacheSearchesUseCase cacheSearchesUseCase;

    @Autowired
    private RestTemplate restTemplate;

    /* instancia o restTemplate */
    CoreZipcodeUseCase(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ResponseEntity<String> search(String uf, String city, String street) {
        var gsonResult = new Gson();
        try {
            if (cityUseCase.validationUfAndCity(uf, city)) {
                HttpEntity<Zipcode> resultHttpEntity = new HttpEntity<>(Zipcode.builder().build());
                String[] args = {uf, city, street, "json"};
                String finalUrl = urlFinalBuilder(urlRoot, args);
                /* caso já exista responde o que já na base */
                var cachesSearches = cacheSearchesUseCase.findByUrl(finalUrl);
                if (cachesSearches.isEmpty()) {
                    ResponseEntity<String> resultResponse;
                    resultResponse = restTemplate.exchange(
                            finalUrl,
                            HttpMethod.GET,
                            resultHttpEntity,
                            String.class);
                    var zipcodes = gsonResult.fromJson(resultResponse.getBody(), Zipcode[].class);
                    if ((resultResponse.getStatusCode().value() == 200) && (validateContent(zipcodes))) {
                        /* se não existe histórico desta busca, então armazena */
                        zipcodeUseCase.saveZipcodeAndSendToKafka(finalUrl, zipcodes);
                        return resultResponse;
                    } else {
                        /* não encontrou conteúdo para a requisição * (204) */
                        return new ResponseEntity<>(AnswersEnum.NOT_FOUND.getMessage(), HttpStatus.NOT_FOUND);
                    }
                } else {
                    ArrayList<Zipcode> zipcodeList = new ArrayList<>();
                    for (ZipcodeDTO zipcodeDTO : cachesSearches.get().getZipcodeDTOS()) {
                        Optional<Zipcode> optionalZipcode = zipcodeUseCase.findById(zipcodeDTO.getObjectId());
                        /* se há conteúdo no objeto Option então adiociona à lista */
                        optionalZipcode.ifPresent(zipcodeList::add);
                    }
                    /* havendo já no histórico de dados, retorna o que está armazenado, sem a necessidade refazer a requisição * (200) */
                    return new ResponseEntity<>(gsonResult.toJson(zipcodeList, List.class), HttpStatus.OK);
                    /* TODO; verificar porque devolve  json como texto ao client */
                }
            } else {
                /* quando as configurações básicas de busca não são atendidas retorna essa informação. * (412) */
                return new ResponseEntity<>(AnswersEnum.CITY_UF_NOT_REGISTERED.getMessage(), HttpStatus.PRECONDITION_FAILED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }

    private String urlFinalBuilder(String url, String[] args) {
        String urlFinal = url;
        for (String arg : args) {
            urlFinal = urlFinal.concat("/".concat(arg));
        }
        return urlFinal;
    }

    private Boolean validateContent(Zipcode[] payload) {
        var result = (payload.length > 0);
        for (Zipcode zipcode : payload) {
            result = !zipcode.getCep().isEmpty() &&
                    !zipcode.getLocalidade().isEmpty() &&
                    (!Objects.requireNonNull(zipcode.getLogradouro()).isEmpty());
            if (!result) {
                break;
            }
        }
        return result;
    }

}

package com.ms.zipcode.usecase;

import com.ms.zipcode.domain.CachesSearches;
import com.ms.zipcode.domain.Resume;
import com.ms.zipcode.domain.Zipcode;
import com.ms.zipcode.domain.dto.ZipcodeDTO;
import com.ms.zipcode.domain.dto.ZipcodeIdDTO;
import com.ms.zipcode.storage.CacheSearchesStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CacheSearchesUseCase {
    @Autowired
    private CacheSearchesStorage storage;
    @Autowired
    private ResumeUseCase resumeUseCase;

    public void saveCache(CachesSearches cachesSearches) {
        storage.save(cachesSearches);
    }

    public List<CachesSearches> findAll() {
        return (List<CachesSearches>) storage.findAll();
    }

    public Optional<CachesSearches> findByUrl(String url) {
        return storage.findByUrl(url);
    }

    public void cachePurge() {
        storage.deleteAll();
    }

    public void update(long id, List<ZipcodeDTO> dto) {
        Optional<CachesSearches> cachesSearches = storage.findById(id);
        if (cachesSearches.isPresent()) {
            cachesSearches.get().setZipcodeDTOS(dto);
            storage.save(cachesSearches.get());
        }
    }

    /*  este método limpa o cache e atualiza conforme as collections */
    public void refreshCache(List<Zipcode> zipcodeList) {
        cachePurge();
        Random random = new Random();
        var uf = "";
        var city = "";
        var street = "";
        var idCache = 1L;

        List<ZipcodeDTO> zipcodeDTOList = new ArrayList<>();
        for (Zipcode zipcode : zipcodeList) {
            Optional<Resume> resume = resumeUseCase.findByResumeId(zipcode.get_id().toString());

            ZipcodeDTO dto = ZipcodeDTO.builder()
                    .zipcodeId(
                            ZipcodeIdDTO.builder()
                                    .uf(zipcode.getUf())
                                    .street(zipcode.getLogradouro())
                                    .city(zipcode.getLocalidade()).build()
                    )
                    .district(zipcode.getBairro())
                    .zipcode(zipcode.getCep())
                    .objectId(zipcode.get_id().toString())
                    .id(parseCustom(zipcode.getCep()))
                    .build();

            if (!uf.equals(zipcode.getUf()) || !city.equals(zipcode.getLocalidade()) || !Objects.equals(street, zipcode.getLogradouro())) {
                zipcodeDTOList.clear();
                zipcodeDTOList.add(dto);

                idCache = random.nextLong();
                uf = zipcode.getUf();
                city = zipcode.getLocalidade();
                street = zipcode.getLogradouro();

                saveCache(CachesSearches.builder()
                        .url(resume.isPresent() ? resume.get().getUrl() : "")
                        .id(idCache)
                        .zipcodeDTOS(zipcodeDTOList)
                        .build());

            } else {
                zipcodeDTOList.add(dto);
                update(idCache, zipcodeDTOList);
            }
        }
    }

    protected int parseCustom(String value) {
        return Integer.parseInt(value.replace("-", ""));
    }
}

package com.ms.zipcode.usecase;

import com.google.gson.Gson;
import com.ms.zipcode.configuration.ResourcesConfig;
import com.ms.zipcode.domain.City;
import com.ms.zipcode.storage.CityStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class CityUseCase {
    @Autowired
    private CityStorage storage;
    @Autowired
    private ResourcesConfig resourcesConfig;

    @PostConstruct
    public void initLoadData() {
        resourcesConfig.loadCityData();
    }

    public boolean validationUfAndCity(String uf, String city) {
        Optional<City> cityResult = storage.findByName(uf, city);
        return cityResult.isPresent();
    }

    public void save(String newCity) {
        Gson gson = new Gson();
        City city = gson.fromJson(newCity, City.class);
        storage.save(city);
    }

}

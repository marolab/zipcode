package com.ms.zipcode.usecase;

import com.ms.zipcode.domain.Resume;
import com.ms.zipcode.storage.ResumeStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ResumeUseCase {
    @Autowired
    private ResumeStorage storage;

    public List<Resume> findAll() {
        return storage.findAll();
    }

    public void save(Resume resume) {
        storage.save(resume);
    }

    public Optional<Resume> findById(String id) {
        return storage.findById(id);
    }

    public Optional<Resume> findByUrl(String url) {
        return storage.findByUrl(url);
    }

    public Optional<Resume> findByResumeId(String resumeId) {
        return storage.findByResumeId(resumeId);
    }

}

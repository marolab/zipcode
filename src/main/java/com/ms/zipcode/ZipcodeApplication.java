package com.ms.zipcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

/* habilitando para usar o Redis (cache) */
@EnableCaching
/* habilitando para usar agendamento (chron) */
@EnableScheduling
@SpringBootApplication
public class ZipcodeApplication {
	public static void main(String[] args) {
		SpringApplication.run(ZipcodeApplication.class, args);
	}
}

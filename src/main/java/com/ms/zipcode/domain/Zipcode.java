package com.ms.zipcode.domain;

import com.mongodb.lang.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Zipcode {
    @MongoId
    private ObjectId _id;
    private String cep;
    @Nullable
    private String logradouro;
    @Nullable
    private String complemento;
    @Nullable
    private String bairro;
    private String localidade;
    private String uf;
    private String ibge;
    @Nullable
    private String gia;
    private String ddd;
    private String siafi;

}


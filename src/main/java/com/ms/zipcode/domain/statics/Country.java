package com.ms.zipcode.domain.statics;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class Country {

    public enum countries {
        BRAZIL("Brasil"),
        USA("Estados Unidos"),
        ARGENTINA("Argentina");

        private final String reference;
        countries(String reference) {
            this.reference = reference;
        }
        public String getReference() {
            return this.reference;
        }
    }
}

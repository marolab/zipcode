package com.ms.zipcode.domain.statics;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class Ufs {

    public enum federation_states {
        MG("Minas Gerais"),
        RS("Rio Grande do Sul"),
        RJ("Rio de Janeiro"),
        PR("Paraná"),
        SP("São Paulo"),
        SC("Santa Caterian");

        private final String name;

        federation_states(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }
}

package com.ms.zipcode.domain.statics;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class StreetTypes {
    public enum types{
        RUA("rua"),
        AVENIDA("avenida"),
        ESTRADA("estrada"),
        ALAMEDA("alameda"),
        TRAVESSA("travessa"),
        LOGRADOURO("logradouro"),
        LOCALIDADE("localidade"),
        LOTEAMENTO("loteamento"),
        VIA("via"),
        VILA("vila");

        private final String reference;

        types(String reference) {
            this.reference = reference;
        }

        public String getReference() {
            return this.reference;
        }

    }

}

package com.ms.zipcode.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ZipcodeIdDTO {
    private String street;
    private String city;
    private String uf;
}

package com.ms.zipcode.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ZipcodeDTO {
    private int id;
    private ZipcodeIdDTO zipcodeId;
    private String zipcode;
    private String district;
    private String objectId;
}

package com.ms.zipcode.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AnswersEnum {
    CITY_UF_NOT_REGISTERED("UF e/ou cidade não cadastrado à essa busca!"),
    NOT_FOUND("Nenhuma informação encontrada à essa busca!");
    private String message;
}

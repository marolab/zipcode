package com.ms.zipcode.domain;

import com.ms.zipcode.domain.dto.ZipcodeDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("CachesSearches")
public class CachesSearches implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Indexed
    @Column(name = "url")
    private String url;
    @Indexed
    @Column(name = "zipcodeResum")
    private List<ZipcodeDTO> zipcodeDTOS;

}

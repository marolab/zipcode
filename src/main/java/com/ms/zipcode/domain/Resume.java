package com.ms.zipcode.domain;

import com.mongodb.lang.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.Column;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Resume {
    @MongoId
    private ObjectId objectId;
    @Nullable
    @Column(name = "url")
    private String url;
    @Nullable
    @Column(name = "resumeId")
    private String resumeId;
}

package com.ms.zipcode.configuration;

import com.ms.zipcode.usecase.CacheSearchesUseCase;
import com.ms.zipcode.usecase.KafkaUseCase;
import com.ms.zipcode.usecase.ZipcodeUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class CronConfig {

    @Autowired
    private KafkaUseCase kafkaUseCase;
    @Autowired
    private ZipcodeUseCase zipcodeUseCase;
    @Autowired
    private CacheSearchesUseCase cacheSearchesUseCase;

    @PostConstruct
    @Scheduled(cron = "${application.cron-storage.expression}")
    public void refreshCache() {
        cacheSearchesUseCase.refreshCache(zipcodeUseCase.zipcodeFindAll());
    }
}

package com.ms.zipcode.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.zipcode.domain.City;
import com.ms.zipcode.storage.CityStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
public class ResourcesConfig {
    private static String CITIES = "classpath:cities.json";
    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private CityStorage cityStorage;
    private Resource resource;
    private ObjectMapper objectMapper;

    @Bean
    public void loadCityData() {
        try {
            objectMapper = new ObjectMapper();
            cityStorage.save(objectMapper.readValue(getPayload(CITIES), City.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getPayload(String filePath) throws IOException {
        resource = resourceLoader.getResource(filePath);
        File dataFile = resource.getFile();
        return String.join(" ",
                Files.readAllLines(
                        Paths.get(dataFile.getPath()),
                        StandardCharsets.UTF_8));

    }

}
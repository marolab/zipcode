# Micro serviço  - poc - Zipcode
    Micro serviço que irá alimentar (produzir) e também consumir filas no Kafka, armazendo no Mongodb e consultando o banco H2.
    Seus gatilhos serão por end-points e listener de fila.

# Objetivo:

   1. receber a requisição com a url com os parâmetros de:
    - sigla unidade federação
    - nome da cidade
    - nome do logradouro/rua/avenida/estrada
   2. consultar a base (H2) e verifica que se a cidade requisitada se encontra lá.
      1. encontrando: monta a url, complmentando com a UF, cidade e rua, com sufixo /.json e requisita na web (https://viacep.com.br/ws/)
         1. arquivo na base mongodb, a url da requisição e retorno do payload.
      2. alimenta a fila Kafka de url não parametrizada na base h2, a ser consumidas por outro micro serviço.
      3. executa, conforme agendamento, leitura da collection de retorno e produz à outra fila kafka informações de resumo da informações de CEP encotnradas (cidade, uf, rua e número do CEP)

# Stack and tools:
- Java 11
- Mongodb (client: Robo 3T)
- Docker-compose
- Kafka (console: Kafdrop - http://localhost:19000/)
- H2 (console: http://localhost:18080/zipcode/h2-console)
- Lombok
- redis (docker run -it --name redis -p 6379:6379 redis:5.0)

# Modelo de arquitetura:
- utilizado modelo de Clean Architecture

# Requisitos:
- Primeiro: executar no terminal da aplicação o comando: docker-compose up -d (subindo as imagens de Kafka, Mongodb e Redis que serão usadas pela aplicação).

# Utilização:
- Consulta - REQUEST:
  curl --location --request GET 'localhost:18080/zipcode/v1?uf=RS&city=Montenegro&street=Santos Dumont'
- Ajustes de parametrização (cadastro de cidades) - REQUEST:
  curl --location --request POST 'localhost:18080/zipcode/v1/city' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "cityName": "Novo Hamburgo",
  "country": "Brasil",
  "uf": "RS"
  }'
- Consulta Redis:
  - Terminal (cmd): docker exec -it redis bash
    - redis-cli